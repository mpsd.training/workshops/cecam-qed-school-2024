{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c2d28f69",
   "metadata": {},
   "source": [
    "# Optical spectra from real-time QEDFT\n",
    "\n",
    "In this tutorial we will calculate the absorption spectrum of a single benzene molecule strongly coupled to a photon mode within a cavity using time-dependent QEDFT which is a natural extension to the electron-only TDDFT [Casida equation](https://www.octopus-code.org/documentation/13/tutorial/response/optical_spectra_from_casida/). Our aim will be to capture the hallmark of strong light-matter coupling (i.e. the emergence of a Rabi splitting between polaritons) usually identified in linear spectroscopy. For a benzene molecule, we will investigate the resonant coupling of a cavity mode to the $\\pi-\\pi^{*}$ excitation that occurs around $\\sim 7$ eV.\n",
    "\n",
    "\n",
    "\n",
    "# Time-dependent QEDFT:\n",
    "\n",
    "The optical spectra of a strongly coupled light-matter system can be obtained from the time-dependent QEDFT by solving the coupled Maxwell-Kohn-Sham equations\n",
    "\n",
    "$$\n",
    "i\\hbar \\frac{\\partial}{\\partial t} \\varphi_{i}(\\textbf{r},t) = \\left( \\frac{\\hat{\\textbf{p}}^{2}}{2m} + \\underbrace{v(\\textbf{r},t) +  v_{\\textrm{Mxc}}([n,q_{\\alpha}];\\textbf{r},t) }_{v_{\\textrm{KS}}([v,n,q_{\\alpha}];\\textbf{r},t)} \\right)\\varphi_{i}(\\textbf{r},t) ,  \\quad \\textrm{and} \\quad\n",
    "\\left(\\frac{\\partial^{2}}{\\partial t^{2}} + \\omega_{\\alpha}^{2}\\right)  q_{\\alpha}(t) = \\underbrace{ -\\frac{j_{\\alpha}(t)}{\\omega_{\\alpha}} + \\omega_{\\alpha}\\boldsymbol{\\lambda}_{\\alpha}\\cdot \\textbf{R}(t) }_{j_{\\alpha,\\textrm{KS}}(t)}. \n",
    "$$\n",
    "\n",
    "The Kohn-Sham orbitals $\\varphi_{i}(\\textbf{r},t)$ can be used to obtain the electronic density  $n(\\textbf{r},t) = \\sum_{i} |\\varphi_i(\\textbf{r},t)|^2$, and the photon displacement fields $q_{\\alpha}(t)$ with associated mode frequencies $\\omega_{\\alpha}$ can be obtained analytically from mode-resolved equation of motion which is given by\n",
    "$$\n",
    "q_{\\alpha}(t) = q_{\\alpha}(t_{0})\\cos(\\omega_{\\alpha}t) + \\frac{\\dot{q}_{\\alpha}(t_{0})}{\\omega_{\\alpha}}\\sin(\\omega_{\\alpha}t) - \\frac{1}{\\omega_{\\alpha}^{2}}\\int_{t_{0}}^{t}dt'\\sin(\\omega_{\\alpha}(t-t'))j_{\\alpha,\\textrm{KS}}(t') \\, .% \\label{photon-q-soln2}\n",
    "$$\n",
    "The Kohn-Sham potential $v_{\\textrm{KS}}(\\textbf{r},t)$ is made up of the external potential $v(\\textbf{r},t)$ and the mean-field exchange-correlation potential $v_{\\textrm{Mxc}}(\\textbf{r},t)$ which can be separated into $v_{\\textrm{Mxc}}(\\textbf{r},t)=v_{\\textrm{Hxc}}(\\textbf{r},t) + v_{\\textrm{pxc}}(\\textbf{r},t)$ where $v_{\\textrm{Hxc}}(\\textbf{r},t)$ and $v_{\\textrm{pxc}}(\\textbf{r},t)$ are respectively the Hatree exchange-correlation and electron-photon exchange-correlation potentials. The mode-resolved Maxwell equation couples to the Kohn-Sham equation via the electronic dipole $\\textbf{R}(t)$ and $\\boldsymbol{\\lambda}_{\\alpha}$ represents the light-matter coupling strength. For the calculations in this tutorial, we set the external current $j_{\\alpha}(t)=0$ and the perturbation comes only from $v(\\textbf{r},t)$.\n",
    "\n",
    "As a last remark, we note that in the decoupling limit between light and matter (i.e. when $\\boldsymbol{\\lambda}_{\\alpha} \\rightarrow 0$), the Maxwell-Kohn-Sham equations decouples to the electron-only Kohn-Sham equation since $v_{\\textrm{Mxc}}(\\textbf{r},t) \\rightarrow v_{\\textrm{Hxc}}(\\textbf{r},t)$.\n",
    "\n",
    "\n",
    "## References\n",
    "For details about time-dependent QEDFT, refer to the following:\n",
    "\n",
    "[1] Davis M. Welakuh, [Ab initio Strong Light-Matter Theoretical Framework for Phenomena in Non-relativistic Quantum Electrodynamics](https://ediss.sub.uni-hamburg.de/handle/ediss/9069).\n",
    "\n",
    "[2] M. Ruggenthaler, J. Flick et al., *Quantum-electrodynamical density-functional theory: Bridging quantum optics and electronic-structure theory* [Phys. Rev. A 90, 012508 (2014)](https://doi.org/10.1103/PhysRevA.90.012508).\n",
    "\n",
    "[3] J. Flick, M. Ruggenthaler, H. Appel, and A. Rubio, *Kohn–Sham approach to quantum electrodynamical density-functional theory: Exact time-dependent effective potentials in real space* [Proc.Natl. Acad. Sci. U.S.A. 112, 15285 (2015)](https://doi.org/10.1073/pnas.151822411).\n",
    "\n",
    "[4] Johannes Flick and P. Narang., *Cavity-Correlated Electron-Nuclear Dynamics from First Principles*, [Phys. Rev. Lett. 121, 113002 (2018)](https://doi.org/10.1103/PhysRevLett.121.113002).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1ab9298",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.gridspec as gridspec"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29e0e338",
   "metadata": {},
   "source": [
    "## Ground-state\n",
    "\n",
    "Our first step will be the calculation of the uncoupled electronic ground state to obtain the occupied Kohn-Sham orbitals and the ground-state electronic density. We will use the following input file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e30514f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_gs.txt'\n",
    "#stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e400add",
   "metadata": {},
   "source": [
    "Now, we can run **octopus** with the following line to obtain the ground-state properties of the single benzene molecule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0258791d",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus > out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3821058",
   "metadata": {},
   "source": [
    "Note that for the benzene molecule 30 valence electrons are explicitly considered amounting to 15 occupied orbitals written in the directory `restart/gs`, while the core atoms are considered implicitly by LDA Troullier-Martins pseudopotentials. Note that the variable [UnitsOutput](https://octopus-code.org/documentation/13/variables/execution/units/unitsoutput/) is commented out because in this tutorial we want to work in atomic units (au). You can take a look at the eigenvalues of the occupied states in the file `static/info`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02cd4eae",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat static/info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4d82f1a",
   "metadata": {},
   "source": [
    "An advantage that the real-time QEDFT has over the electron-photon Casida approach is that requires only the occupied orbitals and no unoccupied orbitals. Also, the Maxwell-Kohn-Sham is exact to all orders and the different orders can be extracted as we will do for the linear-response example."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2cb5dd4d-fa47-47b3-96aa-43795f36b595",
   "metadata": {},
   "source": [
    "##  Real-time TDDFT Calculation\n",
    "\n",
    "First we compute the absorption spectrum of the uncoupled molecule by performing a real-time TDDFT calculation. This is necessary when we want to investigate the Rabi splitting of a specific excitation without knowing *apriori* $\\;$ the details of the molecule's free-space absorption spectra. Also, we do this calculation to later compare to the real-time QEDFT calculation that hightlights the emergence of the Rabi splitting of the $\\pi-\\pi^{*}$ excitation.\n",
    "\n",
    "To calculate the absorption of the benzene molecule in free-space, we excite the system with an infinitesimal electric-field pulse, and then propagate the time-dependent Kohn-Sham equations for a certain time ''T''. Now modify the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) from `gs` to `td` for the time propagation and add the lines bewlow `ConvRelDens`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2e99b7d0-fa99-4f60-877c-d93a37b2814f",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_td.txt'\n",
    "#stderr = 'stderr_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8\n",
    "\n",
    "FromScratch = yes\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.08\n",
    "TDMaxSteps = 6250\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "TDPolarizationDirection = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7dc43a6c-a742-4b0f-8e1b-55f444a54135",
   "metadata": {},
   "source": [
    "We have introduced the following input variables for the time-propagation:\n",
    "* [TDPropagator](https://octopus-code.org/documentation/14/variables/time-dependent/propagation/tdpropagator/): for the time-evolution we use the Approximately Enforced Time-Reversal Symmetry (aetrs) propagator\n",
    "\n",
    "* [TDTimeStep](https://www.octopus-code.org/documentation/14/variables/time-dependent/propagation/tdtimestep/): sets the time-step chosen such that the propagation remains numerically stable.\n",
    "\n",
    "* [TDMaxSteps](https://www.octopus-code.org/documentation//14/variables/time-dependent/propagation/tdmaxsteps): sets the maximum number of time steps to reach the final finite time ''T''.\n",
    "\n",
    "* [TDDeltaStrength](https://www.octopus-code.org/documentation//14/variables/time-dependent/response/tddeltastrength): this is the strength of the perturbation. This number should be small to keep the response linear, but should be sufficiently large to avoid numerical problems.\n",
    "\n",
    "* [TDPolarizationDirection](https://www.octopus-code.org/documentation//14/variables/time-dependent/response/dipole/tdpolarizationdirection): this variable sets the polarization of our perturbation to be on the first axis (''x'').\n",
    "\n",
    "* [FromScratch]() = ```yes```. This will be useful if you decide to run the propagation for other polarization directions.\n",
    "\n",
    "Now, we can run **octopus** with the following line to obtain the real-time linear-response properties of the single benzene molecule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8cd05e5d-0e93-4727-ab14-7016080d426a",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus > out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8aded31b-739f-4c91-b476-8b7aaf08b1c5",
   "metadata": {},
   "source": [
    "A new directory will appear named `td.general` which contains real-time data of the system. For example, you can find the file `td.general/multipoles` which provides the results to obtain the free-space absorption spectra of the molecule. Rename the `td.general` directory to `td.general_el` to indicate it is an electron-only data and also rename the file `td.general_el/multipoles` to `td.general_el/multipoles_x` with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1891d76-4914-4d45-8b3e-1973f82e0052",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv td.general td.general_el\n",
    "!mv td.general_el/multipoles td.general_el/multipoles_x\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ccdb9bd8",
   "metadata": {},
   "source": [
    "\n",
    "##  Real-time QEDFT Calculation\n",
    "\n",
    "We are now set to calculate the optical spectrum of a benzene molecule where a single photon mode is resonantly coupled to the $\\pi-\\pi^{*}$ transition energy. To calculate the absorption, we in the same way as the TDDFT calculation, excite the system with an infinitesimal electric-field pulse, and then propagate the time-dependent coupled Maxwell-Kohn-Sham equations for a certain time ''T''. We assume that we have already obtained required results from the TDDFT calculation which is necessary for the QEDFT calculation. This means we have deduced the $\\pi-\\pi^{*}$ transition energy of $6.936$ eV ($0.2549$ Hartree) from the TDDFT calculation. Note that this can be also deduced from the electron-only Casida or Sterheimer approaches.\n",
    "\n",
    "Now keep the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) set to `td` for the QEDFT time-propagation and add the lines below `TDPolarizationDirection` in the inp file.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06c25d1c",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_td.txt'\n",
    "#stderr = 'stderr_td.txt'\n",
    "\n",
    "CalculationMode = td\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8\n",
    "\n",
    "FromScratch = yes\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDTimeStep = 0.08\n",
    "TDMaxSteps = 6250\n",
    "TDDeltaStrength = 0.01/angstrom\n",
    "TDPolarizationDirection = 1\n",
    "\n",
    "EnablePhotons = yes\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "%PhotonModes\n",
    " 0.254911257 | 0.01 | 1 | 0 | 0\n",
    "%\n",
    "\n",
    "%TDOutput\n",
    " multipoles\n",
    " photons_q\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "484adff7",
   "metadata": {},
   "source": [
    "The variable `EnablePhotons = yes` instructs octopus to enable the coupling to photons and the `ExperimentalFeatures = yes` must be set since the electron-photon setup is a relatively new feature. The [PhotonModes](https://www.octopus-code.org/documentation/main/variables/hamiltonian/xc/photonmodes/) block takes input variables in the following structure: $|\\omega_{\\alpha}|\\lambda_{\\alpha}|\\textbf{e}_{x}|\\textbf{e}_{y}|\\textbf{e}_{z}|$. For the above input file it means the cavity mode has a frequency $\\omega_{\\alpha}=0.255$ Hartree, the light-matter coupling has a strength $\\lambda_{\\alpha}=0.01$ au, and the photon mode is polarized only in the $x$-direction. The block [TDOutput](https://www.octopus-code.org/documentation/12/variables/time-dependent/td_output/tdoutput/) specifiels the different quantities that **octopus** should output: `multipoles` (gives the time-dependent dipole moments) and `photons_q` (gives the photon displacement fields $q_{\\alpha}(t)$).\n",
    "\n",
    "Now, rerun **Octopus** and a new directory will appear again named `td.general` which contains data of the coupled light-matter system. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c76c121",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus > out.log"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8279d7eb-7d14-4408-9781-6b9b062ecd0a",
   "metadata": {},
   "source": [
    "You can find the new file `td.general/multipoles` which provides the results to obtain the absorption spectra. Rename the `td.general` directory to `td.general_el_pt` to indicate it is an electron-photon data and also rename the file `td.general_el_pt/multipoles` to `td.general_el_pt/multipoles_x` with the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d6cad99c",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv td.general td.general_el_pt\n",
    "!mv td.general_el_pt/multipoles td.general_el_pt/multipoles_x\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f88faea",
   "metadata": {},
   "source": [
    "You can take a look at the output of the time-dependent QEDFT calculation of the coupled system as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37e73727",
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 50 td.general_el_pt/multipoles_x"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9cd3084d",
   "metadata": {},
   "source": [
    "The first column is the number of iteration steps, the second column is the time, the third column is the total electronic charge which for the benzene molecule should be $N_{e} \\approx 30$ electrons. The remain columns are the expectation values of the three components of the electronic dipole moment. Note how the dipole along the ''x'' direction (forth column) changes in response to the perturbation while the other components are not affected. If we wanted the external perturbation to be along either $y$- and $z$-directions, then we will respectively set `TDPolarizationDirection = 2` and `TDPolarizationDirection = 3`. For the purpose of this tutorial, we will continue with the perturbation only in the $x$-direction.\n",
    "\n",
    "Another file generated is the `td.general_el_pt/photons_q` which contains information about the photon degrees of the coupled light-matter system. You can take a look at the file where the first column is the number of iteration steps, the second column is the time, the third column is the time-dependent photon displacement field $q_{\\alpha}(t)$ and fourth column the corresponding conjugate momenttum $p_{\\alpha}(t)$. The remain columns are not needed in this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c377506c-1891-4bfc-ac6b-07afd9aa675a",
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 50 td.general_el_pt/photons_q"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65716717",
   "metadata": {},
   "source": [
    "##  Absorption spectrum\n",
    "\n",
    "Now, we will see how to determine the absorption spectrum from the time-dependent information in `td.general/multipoles_x`. The dynamic polarizability is related to the optical absorption cross-section via $\\sigma \\left( \\omega \\right) = \\frac{4 \\pi \\omega}{c} \\mathrm{Im}\\ \\alpha \\left( \\omega \\right) $ in atomic units, or more generally $4 \\pi \\omega \\tilde{\\alpha}\\ \\mathrm{Im}\\ \\alpha \\left( \\omega \\right) $ (where $\\tilde{\\alpha}$ is the fine-structure constant) or $\\frac{\\omega e^2}{\\epsilon_0 c} \\mathrm{Im}\\ \\alpha \\left( \\omega \\right) $. The cross-section is related to the strength function by $S \\left( \\omega \\right) = \\frac{mc}{2 \\pi^2 \\hbar^2} \\sigma \\left( \\omega \\right)$.\n",
    "\n",
    "Also, from the real-time photon displacement field $q_{\\alpha}(t)$, we compute its corresponding spectra $q_{\\alpha}(\\omega)$ by taking the imaginary part of its Fourier transform in frequency space\n",
    "obtained from: $q_{\\alpha}(\\omega) = \\frac{1}{k} \\int_{0}^{\\infty} dt \\, q_{\\alpha}(t) e^{i (\\omega + i\\eta)t}$, where $k$ is the kick strength, and $\\eta$ is the artificial broadening parameter.\n",
    "\n",
    "The spectra for $S(\\omega)$ ($x$ component only) and $q_{\\alpha}(\\omega)$ is obtained by running the python script below. It deduces the absorption spectra of the electron-only and electron-photon systems using the files `td.general_el/multipoles_x` and `td.general_el_pt/multipoles_x`, respectively. The generated plot shows a splitting of the excitation at around $7$ eV into a lower and upper polariton."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eecbf8f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# INPUT DATA\n",
    "\n",
    "au_to_eV = 27.2113845\n",
    "\n",
    "eta = 0.003675\n",
    "c0 = 137.035999679\n",
    "kick_str = 0.005291772086\n",
    "\n",
    "omega_min = 0.0\n",
    "omega_max = 0.7\n",
    "d_omega = 0.000367493\n",
    "omega = np.arange(omega_min, omega_max, d_omega)\n",
    "omega_eV = omega*au_to_eV\n",
    "\n",
    "\n",
    "# LOAD & EXTRACT DATA\n",
    "\n",
    "el_data_mult = np.loadtxt(\"td.general_el/multipoles_x\", skiprows=16)\n",
    "el_pt_data_mult = np.loadtxt(\"td.general_el_pt/multipoles_x\", skiprows=16)\n",
    "el_pt_data_pt_q = np.loadtxt(\"td.general_el_pt/photons_q\", skiprows=4)\n",
    "\n",
    "el_pt_time_t = el_pt_data_mult[:, 1]\n",
    "el_pt_dt = el_pt_time_t[1] - el_pt_time_t[0]\n",
    "el_pt_photons_q_t = el_pt_data_pt_q[:, 2]\n",
    "el_dipole_t = el_data_mult[:, 3] + el_data_mult[:, 4] + el_data_mult[:, 5]\n",
    "el_pt_dipole_t = el_pt_data_mult[:, 3] + el_pt_data_mult[:, 4] + el_pt_data_mult[:, 5]\n",
    "\n",
    "\n",
    "# Function to compute dipole Strength function and Fourier transform of photon coordinate\n",
    "\n",
    "def strength_function(omega, dipole_t, time_t, dt, c0, kick_str, eta):\n",
    "\n",
    "    cross_section = np.zeros(len(omega))\n",
    "    \n",
    "    for ww in range(len(omega)):\n",
    "        cross_section[ww] = -((4.0*np.pi*omega[ww])/c0)*(1.0/kick_str) * \\\n",
    "            np.imag(dipole_t *\n",
    "                np.exp(1j*(omega[ww] + 1j*eta)*time_t)).sum()*dt\n",
    "\n",
    "    dipole_strength = (c0/(2*np.pi**2))*cross_section\n",
    "    \n",
    "    return dipole_strength\n",
    "\n",
    "def FT_photons_q(omega, photon_q_t, time_t, dt, kick_str, eta):\n",
    "\n",
    "    photon_q_w = np.zeros(len(omega))\n",
    "    \n",
    "    for ww in range(len(omega)):\n",
    "        photon_q_w[ww] = -(1.0/kick_str) * \\\n",
    "            np.imag(photon_q_t *\n",
    "                np.exp(1j*(omega[ww] + 1j*eta)*time_t)).sum()*dt\n",
    "    \n",
    "    return photon_q_w\n",
    "\n",
    "\n",
    "# Compute dipole Strength function\n",
    "\n",
    "file_name = 'data_computed_spectra.txt'\n",
    "\n",
    "el_strn_fxn = np.zeros(len(omega))\n",
    "el_pt_strn_fxn = np.zeros(len(omega))\n",
    "el_pt_photons_q_w = np.zeros(len(omega))\n",
    "\n",
    "if os.path.isfile(file_name):\n",
    "    print()\n",
    "    print('... loading computed data')\n",
    "    #el_computed_spectra = np.loadtxt(file_name_el)\n",
    "    el_pt_computed_spectra = np.loadtxt(file_name)\n",
    "    el_pt_strn_fxn = el_computed_spectra[:, 0]\n",
    "    el_pt_strn_fxn = el_pt_computed_spectra[:, 1]\n",
    "    el_pt_photons_q_w = el_pt_computed_spectra[:, 2]\n",
    "else:\n",
    "    print()\n",
    "    print('... no computed data found')\n",
    "    print('... computing absorption spectrum')\n",
    "    \n",
    "    el_strn_fxn = \\\n",
    "        strength_function(omega, el_dipole_t, el_pt_time_t, el_pt_dt, c0, kick_str, eta)\n",
    "    \n",
    "    el_pt_strn_fxn = \\\n",
    "        strength_function(omega, el_pt_dipole_t, el_pt_time_t, el_pt_dt, c0, kick_str, eta)\n",
    "\n",
    "    el_pt_photons_q_w = \\\n",
    "        FT_photons_q(omega, el_pt_photons_q_t, el_pt_time_t, el_pt_dt, kick_str, eta)\n",
    "\n",
    "    np.savetxt(file_name, np.c_[el_strn_fxn, el_pt_strn_fxn, el_pt_photons_q_w])\n",
    "\n",
    "\n",
    "    \n",
    "# PLOT RESULT\n",
    "\n",
    "gs = gridspec.GridSpec(2, 1)\n",
    "gs.update(hspace=0.1)\n",
    "fig = plt.figure(figsize=(5, 6))\n",
    "# first plot: \n",
    "ax1 = fig.add_subplot(gs[0])\n",
    "ax1.plot(omega_eV, el_strn_fxn, 'red', linestyle='-')\n",
    "ax1.plot(omega_eV, el_pt_strn_fxn, 'blue', linestyle='--')\n",
    "ax1.legend([r\"electron-only\", r\"electron-photon\"],\n",
    "          loc='upper center', ncol=3, bbox_to_anchor=(0.5, 1.25), prop={'size': 10})\n",
    "ax1.margins(x=0)\n",
    "ax1.xaxis.set_visible(False)\n",
    "ax1.text(0.5, 100, \"(a)\", fontsize=15)\n",
    "ax1.set_ylabel(r\"$S_{x}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax1.get_yaxis().set_label_coords(-0.13, 0.5)\n",
    "ax1.tick_params(axis='both', which='major', labelsize=13)\n",
    "# second plot: \n",
    "ax2 = fig.add_subplot(gs[1])\n",
    "ax2.plot(omega_eV, el_pt_photons_q_w, 'dodgerblue', linestyle='-')\n",
    "ax2.margins(x=0)\n",
    "ax2.text(0.5, 150, \"(b)\", fontsize=15)\n",
    "ax2.set_ylabel(r\"$q_{\\alpha}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax2.get_yaxis().set_label_coords(-0.17, 0.5)\n",
    "ax2.set_xlabel(r\"$\\hbar\\omega$ (eV)\", fontsize=15)\n",
    "ax2.tick_params(axis='both', which='major', labelsize=13)\n",
    "plt.savefig('abs_spectrum_benzene.png', bbox_inches = \"tight\", dpi=200)\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97ce1db6",
   "metadata": {},
   "source": [
    "Figures (a), (b) and (c) shows respecively the different components of the dipole strength functions in the $x$-, $y$- and $z$-directions. In figure (a) we find a Rabi splitting into lower and upper polaritons due to the resonance coupling of a photon mode to the $\\pi - \\pi^{*}$ excitation energy. No polariton splitting emerges in figures (b) and (c) since the photon mode is polarized only in the $x$-direction. \n",
    "\n",
    "To see a larger Rabi splitting, you can rerun the electron-photon Casida calculation and only changing the light-matter coupling parameter, for example $\\lambda_{\\alpha}=0.03, 0.05, 0.07, 0.09$ au."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "554d4f1d",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
